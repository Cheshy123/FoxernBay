
/client
	var/datum/media_manager/media

/client/New()
	..()
	media = new (src)
	media.stop_music()

/datum/media_manager
	var/url = ""				// URL of currently playing media
	var/start_time = 0			// world.time when it started playing *in the source* (Not when started playing for us)
	var/source_volume = 1		// Volume as set by source. Actual volume = "volume * source_volume"
	var/rate = 1				// Playback speed.  For Fun(tm)
	var/volume = 1				// Client's volume modifier. Actual volume = "volume * source_volume"
	var/client/owner			// Client this is actually running in
	var/const/WINDOW_ID = "rpane.mediapanel"	// Which elem in skin.dmf to use

/datum/media_manager/New(var/client/C)
	ASSERT(istype(C))
	src.owner = C

	show_browser(owner, null, "window=[WINDOW_ID]")
	show_browser(owner, PLAYER_HTML5_HTML, "window=[WINDOW_ID]")

/datum/media_manager/Destroy()

	show_browser(owner, null, "window=[WINDOW_ID]")
	owner.media = null

	return ..()

/datum/media_manager/proc/send_update()
	send_output(owner, list2params(list(url, (world.time - start_time) / 10, volume * source_volume)), "[WINDOW_ID]:SetMusic")

/datum/media_manager/proc/push_music(var/targetURL, var/targetStartTime, var/targetVolume)
	if (url != targetURL || abs(targetStartTime - start_time) > 1 || abs(targetVolume - source_volume) > 0.1 /* 10% */)
		url = targetURL
		start_time = targetStartTime
		source_volume = Clamp(targetVolume, 0, 1)
		send_update()

/datum/media_manager/proc/stop_music()
	push_music("", 0, 1)

/datum/media_manager/proc/update_volume(var/value)
	if(volume != value)
		volume = value
		send_update()

/datum/media_manager/proc/update_source_volume(var/value)
	if(source_volume != value)
		source_volume = value
		send_update()