/datum/track
	var/url			// URL to load song from
	var/title		// Song title
	var/artist		// Song's creator
	var/duration	// Song length in deciseconds
	var/secret		// Show up in regular playlist or secret playlist?
	var/lobby		// Be one of the choices for lobby music? TODO: Implement?

/datum/track/New(var/url, var/title, var/duration, var/artist = "", var/secret = 0, var/lobby = 0)
	src.url = url
	src.title = title
	src.artist = artist
	src.duration = duration
	src.secret = secret
	src.lobby = lobby

/proc/createTrackListFromJSON(var/file)
	var/list/datum/track/trackList = list()
	if(!fexists(file))
		warning("File not found: [file]")
		return null
	var/list/jsonData = json_decode(file2text(file))
	if(!istype(jsonData))
		warning("Failed to read tracks from [file], json_decode failed.")
	for(var/entry in jsonData)
		if(!istext(entry["url"]))
			warning("[file] entry [entry]: bad or missing 'url'")
			continue
		if(!istext(entry["title"]))
			warning("[file] entry [entry]: bad or missingg 'title'")
			continue
		if(!isnum(entry["duration"]))
			warning("[file] entry [entry]: bad or missing 'duration'")
			continue
		var/datum/track/T = new(entry["url"], entry["title"], entry["duration"])
		if(istext(entry["artist"]))
			T.artist = entry["artist"]
		T.secret = entry["secret"] ? TRUE : FALSE
		T.lobby = entry["lobby"] ? TRUE : FALSE
		trackList += T
	return trackList