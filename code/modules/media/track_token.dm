
/datum/track_token
	var/atom/source          // Where the sound originates from
	var/list/listeners       // Assoc: Atoms hearing this sound, and their sound datum
	var/range                // How many turfs away the sound will stop playing completely

	var/datum/track/track    // Sound datum, holds most sound relevant data
	var/volume = 0          // Volume of sound

	var/start_time = 0

	var/datum/proximity_trigger/square/proxy_listener
	var/list/can_be_heard_from

/datum/track_token/New(var/atom/source, var/datum/track/track, var/range = 4, var/volume = 50)
	..()
	if(!istype(source))
		CRASH("Invalid sound source: [log_info_line(source)]")
	if(!istype(track))
		CRASH("Invalid sound: [log_info_line(track)]")

	src.source = source
	src.track = track
	src.range = range
	start_time = world.time

	listeners = list()

	destroyed_event.register(source, src, /datum/proc/qdel_self)

	if(ismovable(source))
		proxy_listener = new(source, /datum/track_token/proc/PrivAddListener, /datum/track_token/proc/PrivLocateListeners, range, proc_owner = src)
		proxy_listener.register_turfs()

	SetVolume(volume)

/datum/track_token/Destroy()
	Stop()
	return ..()

/datum/track_token/proc/SetVolume(var/new_volume)
	new_volume = Clamp(new_volume, 0, 100)
	if(volume == new_volume)
		return
	volume = new_volume

	for(var/listener in listeners)
		listener:client.media.update_source_volume(new_volume/100)
	PrivUpdateListeners(TRUE)

datum/track_token/proc/Mute()
	return

/datum/track_token/proc/Unmute()
	return

/datum/track_token/proc/Pause()
	return

// Normally called Resume but I don't want to give people false hope about being unable to un-stop a sound
/datum/track_token/proc/Unpause()
	return

/datum/track_token/proc/Stop()
	for(var/listener in listeners)
		PrivRemoveListener(listener)

	qdel_null(proxy_listener)

	ASSERT(listeners.len == 0)
	listeners = null

	destroyed_event.unregister(source, src, /datum/proc/qdel_self)

	source = null

/datum/track_token/proc/PrivLocateListeners(var/list/prior_turfs, var/list/current_turfs)
	can_be_heard_from = current_turfs
	var/current_listeners = all_hearers(source, range)
	var/former_listeners = listeners - current_listeners
	var/new_listeners = current_listeners - listeners

	for(var/listener in former_listeners)
		PrivRemoveListener(listener)

	for(var/listener in new_listeners)
		PrivAddListener(listener)

	for(var/listener in current_listeners)
		PrivUpdateListenerLoc(listener, FALSE)

datum/track_token/proc/PrivAddListener(var/atom/listener)
	if(!ismob(listener))
		return

	if(isvirtualmob(listener))
		var/mob/observer/virtual/v = listener
		if(!(v.abilities & VIRTUAL_ABILITY_HEAR))
			return
		listener = v.host
	if(listener in listeners)
		return

	listeners += listener

	moved_event.register(listener, src, /datum/track_token/proc/PrivUpdateListenerLoc)
	destroyed_event.register(listener, src, /datum/track_token/proc/PrivRemoveListener)

	listener:client.media.push_music(track.url, start_time, volume/100)
	PrivUpdateListenerLoc(listener, TRUE)

/datum/track_token/proc/PrivRemoveListener(var/atom/listener)
	if(!ismob(listener))
		return
		
	listener:client.media.stop_music()

	moved_event.unregister(listener, src, /datum/track_token/proc/PrivUpdateListenerLoc)
	destroyed_event.unregister(listener, src, /datum/track_token/proc/PrivRemoveListener)
	listeners -= listener

/datum/track_token/proc/PrivUpdateListenerLoc(var/atom/listener, var/update_sound = FALSE)
	var/turf/source_turf = get_turf(source)
	var/turf/listener_turf = get_turf(listener)

	var/distance = get_dist(source_turf, listener_turf)
	if(!listener_turf || (distance > range) || !(listener_turf in can_be_heard_from))
		PrivRemoveListener(listener)
		return

/datum/track_token/proc/PrivUpdateListeners(var/update_sound = FALSE)
	for(var/listener in listeners)
		PrivUpdateListenerLoc(listener, update_sound)


